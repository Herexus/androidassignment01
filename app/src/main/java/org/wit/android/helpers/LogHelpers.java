package org.wit.android.helpers;

import android.util.Log;

//a LogHelper class mostly taken from work in MyRent Labs
public class LogHelpers
{
    public static void info(Object parent, String message)
    {
        Log.i(parent.getClass().getSimpleName(), message);
    }
}