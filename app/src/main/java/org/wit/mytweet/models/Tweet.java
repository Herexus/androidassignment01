package org.wit.mytweet.models;

import android.content.Context;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.wit.practice.R;

public class Tweet
{
    public UUID id;
    public Date  date;
    public String tweetout;
    public String  tweetwho;
    public int charCount;

// We use JSON in order to serialize the tweets. Each tweet has fields as follows.
    private static final String JSON_ID             = "id"            ;
    private static final String JSON_TWEETOUT    = "tweetout"   ;
    private static final String JSON_DATE           = "date"          ;
    private static final String JSON_TWEETWHO         = "tweetwho";
    private static final String JSON_CHARCOUNT = "charCount";

    public Tweet()
    {
        id = UUID.randomUUID();
        date = new Date();
        tweetwho      = " none ";
        charCount = 140;
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id            = UUID.fromString(json.getString(JSON_ID));
        tweetout = json.getString(JSON_TWEETOUT);
        date          = new Date(json.getLong(JSON_DATE));
        tweetwho        = json.getString(JSON_TWEETWHO);
        charCount = json.getInt(JSON_CHARCOUNT);
    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , id.toString());
        json.put(JSON_TWEETOUT   , tweetout);
        json.put(JSON_DATE          , date.getTime());
        json.put(JSON_TWEETWHO        , tweetwho);
        json.put(JSON_CHARCOUNT,   charCount);
        return json;
    }

    // used when trying to get the current date. Calls current date from device memory.
    public String getDateString()
    {
        return "Tweeted at: " + DateFormat.getDateTimeInstance().format(date);
    }

    public String getTweetReport(Context context)
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, date).toString();
        String newTweeter = tweetwho;
        if (tweetwho == null)
        {
            newTweeter = context.getString(R.string.tweet_report_nobody_tweeting);
        }
        else
        {
            newTweeter = context.getString(R.string.tweet_report_tweeter, tweetwho);
        }
        String report =  "Tweet: " + tweetout + " Date: " + dateString +  " " + newTweeter;
        return report;
    }
}