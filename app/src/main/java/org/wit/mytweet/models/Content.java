package org.wit.mytweet.models;

import java.util.ArrayList;
import java.util.UUID;
import static org.wit.android.helpers.LogHelpers.info;

import android.util.Log;

// the Content class is used to save tweets and to save and load tweets.
public class Content
{
    public  ArrayList<Tweet>  tweets;
    private ContentSerializer serializer;

    public Content(ContentSerializer serializer)
    {
        this.serializer = serializer;
        try
        {
            tweets = serializer.loadTweets();
        }
        catch (Exception e)
        {
            info(this, "Error loading your tweets: " + e.getMessage());
            tweets = new ArrayList<Tweet>();
        }
    }

    public boolean saveTweets()
    {
        try
        {
            serializer.saveTweets(tweets);
            info(this, "Tweets saved to file");
            return true;
        }
        catch (Exception e)
        {
            info(this, "Error saving tweets: " + e.getMessage());
            return false;
        }
    }

    // a method for adding a tweet to an array of tweets.

    public void addTweet(Tweet tweet)
    {
        tweets.add(tweet);
    }

    public Tweet getTweet(UUID id)
    {
        Log.i(this.getClass().getSimpleName(), "UUID parameter id: "+ id);

        for (Tweet twt : tweets)
        {
            if(id.equals(twt.id))
            {
                return twt;
            }
        }
        info(this, "failed to find tweet. returning first element array to avoid crash");
        return null;
    }

    public void deleteTweet(Tweet t)
    {
        tweets.remove(t);
    }

    public void removeAllTweets() {
        tweets.removeAll(tweets);
    }
}