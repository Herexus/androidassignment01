package org.wit.mytweet.app;

import android.app.Application;

import org.wit.mytweet.models.Content;
import org.wit.mytweet.models.ContentSerializer;

import static org.wit.android.helpers.LogHelpers.info;


public class MyTweetApp extends Application
{

        private static final String FILENAME = "content.json";

        public Content content;

        @Override
        public void onCreate()
        {
            super.onCreate();
            ContentSerializer serializer = new ContentSerializer(this, FILENAME);
            content = new Content(serializer);

            info(this, "MyTweet app launched");
        }
}