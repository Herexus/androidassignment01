package org.wit.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;


import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Content;
import org.wit.mytweet.models.Tweet;
import org.wit.practice.R;
import java.util.ArrayList;
import java.util.UUID;

import static org.wit.android.helpers.LogHelpers.info;


public class MyTweetPager extends FragmentActivity implements ViewPager.OnPageChangeListener
{
    private ViewPager viewPager;

    private ArrayList<Tweet> tweets;
    private Content content;

    private PagerAdapter pagerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);

        setTweetList();

        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

        setCurrentItem();
    }
// method to set list of tweets from the Content class array of tweets.
    private void setTweetList()
    {
        MyTweetApp app = (MyTweetApp) getApplication();
        content = app.content;
        tweets = content.tweets;
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2)
    {
        info(this, "onPageScrolled: arg0 " + arg0 + " arg1 " + arg1 + " arg2 " + arg2);
        Tweet tweet = tweets.get(arg0);
        if (tweet.tweetout != null)
        {
            setTitle(tweet.tweetout);
        }
    }

    @Override
    public void onPageSelected(int position)
    {
    }

    @Override
    public void onPageScrollStateChanged(int state)
    {
    }

    private void setCurrentItem()
    {
        UUID twt = (UUID) getIntent().getSerializableExtra(MyTweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++)
        {
            if (tweets.get(i).id.toString().equals(twt.toString()))
            {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter
    {
        private ArrayList<Tweet> tweets;

        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets)
        {
            super(fm);
            this.tweets = tweets;
        }

        @Override
        public int getCount()
        {
            return tweets.size();
        }

        @Override
        public Fragment getItem(int pos)
        {
            Tweet tweet = tweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(MyTweetFragment.EXTRA_TWEET_ID, tweet.id);
            MyTweetFragment fragment = new MyTweetFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }
}